/**
    @filename   :   sdcard_functions.ino
    @brief      :   DigiSketch SD card function sketch
    @author     :   Mike Wielgosz

    Copyright (C) Mike Wielgosz     2019-03-14

   Permission is hereby granted, free of charge, to any person obtaining a copy
   of this software and associated documnetation files (the "Software"), to deal
   in the Software without restriction, including without limitation the rights
   to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
   copies of the Software, and to permit persons to  whom the Software is
   furished to do so, subject to the following conditions:

   The above copyright notice and this permission notice shall be included in
   all copies or substantial portions of the Software.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   FITNESS OR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
   LIABILITY WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
   THE SOFTWARE.
*/

void initSdCard() {
  Serial.println(F("|     Initializing SD card      |"));

  if (!SD.begin(BUILTIN_SDCARD)) {
    Serial.println(F("|    Initialization failed!     |"));
    Serial.println(F("|-------------------------------|"));
    return;
  }
  rootDirExistCheck();
  Serial.println(F("|     SDCARD init complete      |"));
  Serial.println(F("|-------------------------------|"));
}

void rootDirExistCheck() {
  if (!SD.exists(sdPath)) {
    Serial.println(F("|    Creating SD Directory      |"));
    SD.mkdir(sdPath);
  } else {
    Serial.println(F("|      SD Directory Exists      |"));
  }
}

void listSketchFiles() {
  File rootDir = SD.open(sdPath);
  Serial.println(F("| SDCARD file list:             |"));
  Serial.print(F("| - /")); Serial.println(rootDir.name());

  while (true) {
    File entry =  rootDir.openNextFile();
    if (! entry) {
      // no more files
      //Serial.println("**nomorefiles**");
      break;
    }
    //    for (uint8_t i = 0; i < numTabs; i++) {
    //      Serial.print('\t');
    //    }
    Serial.print(F("| - ")); Serial.print(entry.name());
    //    if (entry.isDirectory()) {
    //       Serial.println("/");
    //       printDirectory(entry, numTabs+1);
    //     } else {
    //       // files have sizes, directories do not
    //       Serial.print("\t\t");
    //       Serial.println(entry.size(), DEC);
    //     }
    if (!entry.isDirectory()) {
      // files have sizes, directories do not
      Serial.print("\t\t");
      Serial.println(entry.size(), DEC);
    }
    entry.close();
  }
}

void saveSketchFile() { // TODO: save current X & Y values
  Serial.println(F("|-------------------------------|"));
  Serial.println(F("|    - Saving Sketch to SD -    |"));
  char filePath[30];
  const char *filename = "/TEST.DGK";
  strcpy(filePath, sdPath);
  strcat(filePath, filename);

  SD.remove(filePath);

  File saveFile = SD.open(filePath, FILE_WRITE);
  if (saveFile) {
    byte saveBytes = 0;
    saveBytes += saveFile.write(IMAGE_BUF, sizeof(IMAGE_BUF)); // Sketch
    saveBytes += saveFile.print("x="); saveBytes += saveFile.println(positionX); // X position
    saveBytes += saveFile.print("y="); saveBytes += saveFile.println(positionY); // Y position

    saveFile.close();

    if (DEBUG) {
      Serial.print(F("| Filesize: ")); Serial.print(saveBytes); Serial.println(F(" bytes"));
    }

    Serial.println(F("|       - Sketch Saved -        |"));
    Serial.println(F("|-------------------------------|"));
  } else {
    Serial.println(F("|    - Error saving sketch -    |"));
    Serial.println(F("|-------------------------------|"));
  }
  closeMenu();
}

void loadSketchFile() {
  Serial.println(F("|-------------------------------|"));
  Serial.println(F("|  - Loading Sketch from SD -   |"));
  char filePath[30];
  const char *filename = "/TEST.DGK";
  strcpy(filePath, sdPath);
  strcat(filePath, filename);

  File loadFile = SD.open(filePath, FILE_READ);

  // Load Sketch dump
  loadFile.read(IMAGE_BUF, 16000);

  // Load extra variables
  // TODO: optimize parsing to not scan IMAGE_BUF data
  char buffer[4];
  byte index = 0;
  while (loadFile.available()) {
    char c = loadFile.read();

    // Test for <cr> and <lf>
    if (c == '\n' || c == '\r') {
      parseSaveFile(buffer);
      index = 0;
      buffer[index] = '\0'; // Keep buffer NULL terminated
    } else {
      buffer[index++] = c;
      buffer[index] = '\0'; // Keep buffer NULL terminated
    }
  }

  loadFile.close();

  Serial.println(F("|      - Sketch Loaded -        |"));
  Serial.println(F("|-------------------------------|"));

  closeMenu();
  updateSketchOnEpd(false);
}

void parseSaveFile(char *buff) {
  char *name = strtok(buff, " =");
  if (name) {
    if (strcmp(name, "x") == 0) {
      char *valu = strtok(NULL, " ");
      if (valu) {
        int val = atoi(valu);
        knobX.write(val * dtOffset);
        positionX = val;
      } else if (DEBUG) {
        Serial.println(F("|   !- Error loading sketch-!   "));
      }
    } else if (strcmp(name, "y") == 0) {
      char *valu = strtok(NULL, " ");
      if (valu) {
        int val = atoi(valu);
        knobY.write(val * dtOffset);
        positionY = val;
      } else if (DEBUG) {
        Serial.println(F("|   !- Error loading sketch-!   "));
      }
    }
  }
}
