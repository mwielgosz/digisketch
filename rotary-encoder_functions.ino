/**
    @filename   :   rotary-encoder_functions.ino
    @brief      :   DigiSketch Rotary Encoder function sketch
    @author     :   Mike Wielgosz

    Copyright (C) Mike Wielgosz     2019-01-21

   Permission is hereby granted, free of charge, to any person obtaining a copy
   of this software and associated documnetation files (the "Software"), to deal
   in the Software without restriction, including without limitation the rights
   to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
   copies of the Software, and to permit persons to  whom the Software is
   furished to do so, subject to the following conditions:

   The above copyright notice and this permission notice shall be included in
   all copies or substantial portions of the Software.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   FITNESS OR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
   LIABILITY WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
   THE SOFTWARE.
*/





//const char msg[] = "Ensure Rotary Encoder PINs are on interrupt pins and defined properly";


//void Error ( int encoderNum ) {
//  Serial.print("ERROR creating encoder "); Serial.print(encoderNum); Serial.println("!");
//  Serial.println(msg);
//  Serial.println("Possible malloc error, the rotary ID is incorrect, or too many rotaries");
//  while (1);  // Halt and catch fire
//}
//
//void initRotaryEncoders() {
//  // Define a rotary to go from 0 to 399 in increments of 1. Initialize it to 200. No rollover
//  if (!encoderX.AddRotaryCounter(ROTARY_X_ID1, 200, 0, 399, 1, false)) {
//    Error(1);
//  }
//
//  // Define a rotary to go from 0 to 299 in increments of 1. Initialize it to 150. No rollover
//  if (!encoderY.AddRotaryCounter(ROTARY_Y_ID1, 150, 0, 299, 1, false)) {
//    Error(2);
//  }
//}
/*
   Deprecated
*/
//void initRotaryEncoders_OLD() {
//  // Initialization of the input-pins
//  pinMode (PIN_CLK_X, INPUT);
//  pinMode (PIN_DT_X, INPUT);
//  pinMode (PIN_SW_X, INPUT);
//  pinMode (PIN_CLK_Y, INPUT);
//  pinMode (PIN_DT_Y, INPUT);
//  pinMode (PIN_SW_Y, INPUT);
//
//  // Activating of their pull up resistors
//  digitalWrite(PIN_CLK_X, true);
//  digitalWrite(PIN_DT_X, true);
//  digitalWrite(PIN_SW_X, true);
//  digitalWrite(PIN_CLK_Y, true);
//  digitalWrite(PIN_DT_Y, true);
//  digitalWrite(PIN_SW_Y, true);
//
//  // Initial reading of the PIN_CLK(s)
//  PIN_CLK_LAST_X = digitalRead(PIN_CLK_X);
//  PIN_CLK_LAST_Y = digitalRead(PIN_CLK_Y);
//}

//void loop() {
//  if (encoderX.SwitchPressed()) {
//    Serial.println("Encoder X switch pressed. ");
//
//    if (MENU_OPEN) {
//      MENU_OPEN = false;
//      epd.ClearFrame();
//      updateSketchOnEpd(false);
//    } else {
//      resetPosition();
//    }
//  }
//
//  if (encoderY.SwitchPressed() ) {
//    Serial.println("Encoder Y switch pressed. ");
//
//    // Show menu & pause drawing
//    if (MENU_OPEN) {
//      // Select menu option
//      switch (OPT_SELECTED) {
//        case 0: // RESET
//          resetSketch();
//          break;
//        case 1: // SAVE - TODO
//          break;
//        case 2: // LOAD - TODO
//          break;
//      }
//    } else {
//      // MENU OPEN - from sketch
//      MENU_OPEN = true;
//      UI_UPDATE_LOCK = true; // Ensure UI thread is locked
//      OPT_SELECTED = 0;
//      menuSelectReset(false);
//    }
//  }
//
//  if (encoderX.HasRotaryValueChanged() || encoderY.HasRotaryValueChanged()) {
//    //POSITION_X = encoderX.GetRotaryValue(ROTARY_X_ID1);
//    //POSITION_Y = encoderY.GetRotaryValue(ROTARY_Y_ID1);
//    Serial.print("Encoder X ROTARY_ID1: "); Serial.println(encoderX.GetRotaryValue(ROTARY_X_ID1));
//    Serial.print("Encoder Y ROTARY_ID1: "); Serial.println(encoderY.GetRotaryValue(ROTARY_Y_ID1));
//    Serial.println("---------------------------------------");
//
//    updateSketchPaintBuffer(encoderX.GetRotaryValue(ROTARY_X_ID1), encoderY.GetRotaryValue(ROTARY_Y_ID1));
//  }
//}

/***************************
  X-AXIS movement (KY-40)
***************************/
//void xAxisThread() {
//  while (1) {
//    PIN_CLK_CURRENT_X = digitalRead(PIN_CLK_X);
//    // Check for a Change
//    if (PIN_CLK_CURRENT_X != PIN_CLK_LAST_X) {
//      if (IS_EPD_SLEEP) {
//        initEpd(false);
//      } else if (!IS_EPD_SLEEP && !MENU_OPEN) {
//        IDLE_MSEC = 0;
//        if (digitalRead(PIN_DT_X) != PIN_CLK_CURRENT_X) {
//          // PIN_CLK_X has changed first
//          POSITION_X++;
//          DIRECTION_X = true;
//          // Screen edge detect (x-width)
//          if (POSITION_X > EPD_WIDTH - 1) {
//            POSITION_X = EPD_WIDTH - 1;
//            UI_UPDATE_LOCK = true;
//          } else {
//            updateSketchPaintBuffer();
//          }
//        } else {
//          // Else PIN_DT_X changed first
//          DIRECTION_X = false;
//          POSITION_X--;
//          // Screen edge detect (x-0)
//          if (POSITION_X < 0) {
//            POSITION_X = 0;
//            UI_UPDATE_LOCK = true;
//          } else {
//            updateSketchPaintBuffer();
//          }
//        }
//
//        if (DEBUG) {
//          printPosition();
//        }
//      }
//    }
//
//    // Preparation for the next run:
//    // The current value will be the last value for the next run.
//    PIN_CLK_LAST_X = PIN_CLK_CURRENT_X;
//
//    // Reset funciton to save the current position
//    if (!digitalRead(PIN_SW_X)) {
//      if (MENU_OPEN) {
//        MENU_OPEN = false;
//        epd.ClearFrame();
//        updateSketchOnEpd(false);
//      } else {
//        resetPosition();
//      }
//    }
//
//    /***************************
//      Y-AXIS movement (KY-40)
//    ***************************/
//    PIN_CLK_CURRENT_Y = digitalRead(PIN_CLK_Y);
//    if (PIN_CLK_CURRENT_Y != PIN_CLK_LAST_Y) {
//      if (IS_EPD_SLEEP) {
//        initEpd(false);
//      } else if (!IS_EPD_SLEEP) {
//        IDLE_MSEC = 0;
//        if (digitalRead(PIN_DT_Y) != PIN_CLK_CURRENT_Y) {
//          if (!MENU_OPEN) {
//            // PIN_CLK_Y has changed first
//            POSITION_Y--;
//            DIRECTION_Y = true;
//            // Screen edge detect (y-0)
//            if (POSITION_Y < 0) {
//              POSITION_Y = 0;
//              UI_UPDATE_LOCK = true;
//            } else {
//              updateSketchPaintBuffer();
//            }
//          } else {
//            OPT_SELECTED--;
//            if (OPT_SELECTED < 0) {
//              OPT_SELECTED = 2;
//            }
//            selectMenu();
//          }
//        } else {
//          if (!MENU_OPEN) {
//            // Else PIN_DT_Y changed first
//            DIRECTION_Y = false;
//            POSITION_Y++;
//            // Screen edge detect (y-height)
//            if (POSITION_Y > EPD_HEIGHT - 1) {
//              POSITION_Y = EPD_HEIGHT - 1;
//              UI_UPDATE_LOCK = true;
//            } else {
//              updateSketchPaintBuffer();
//            }
//          } else {
//            // MENU OPEN - change selection
//            OPT_SELECTED++;
//            if (OPT_SELECTED > 2) {
//              OPT_SELECTED = 0;
//            }
//            selectMenu();
//          }
//        }
//
//        if (DEBUG) {
//          printPosition();
//        }
//      }
//    }
//
//    // Preparation for the next run:
//    // The current value will be the last value for the next run.
//    PIN_CLK_LAST_Y = PIN_CLK_CURRENT_Y;
//
//    // Reset funciton to save the current position
//    if (!digitalRead(PIN_SW_Y)) {
//      // Show menu & pause drawing
//      if (MENU_OPEN) {
//        // Select menu option
//        switch (OPT_SELECTED) {
//          case 0: // RESET
//            resetSketch();
//            break;
//          case 1: // SAVE - TODO
//            break;
//          case 2: // LOAD - TODO
//            break;
//        }
//      } else {
//        // MENU OPEN - from sketch
//        MENU_OPEN = true;
//        UI_UPDATE_LOCK = true; // Ensure UI thread is locked
//        OPT_SELECTED = 0;
//        menuSelectReset(false);
//      }
//    }
//  }
//  threads.yield();
//}

//void rotaryEncoderThread() {
//  while (1) {
//    if (encoderX.SwitchPressed()) {
//      Serial.println("Encoder X switch pressed. ");
//
//      if (MENU_OPEN) {
//        MENU_OPEN = false;
//        epd.ClearFrame();
//        updateSketchOnEpd(false);
//      } else {
//        resetPosition();
//      }
//    }
//
//    if (encoderY.SwitchPressed() ) {
//      Serial.println("Encoder Y switch pressed. ");
//
//      // Show menu & pause drawing
//      if (MENU_OPEN) {
//        // Select menu option
//        switch (OPT_SELECTED) {
//          case 0: // RESET
//            resetSketch();
//            break;
//          case 1: // SAVE - TODO
//            break;
//          case 2: // LOAD - TODO
//            break;
//        }
//      } else {
//        // MENU OPEN - from sketch
//        MENU_OPEN = true;
//        UI_UPDATE_LOCK = true; // Ensure UI thread is locked
//        OPT_SELECTED = 0;
//        menuSelectReset(false);
//      }
//    }
//
//    if (encoderX.HasRotaryValueChanged() || encoderY.HasRotaryValueChanged()) {
//      //POSITION_X = encoderX.GetRotaryValue(ROTARY_X_ID1);
//      //POSITION_Y = encoderY.GetRotaryValue(ROTARY_Y_ID1);
//      Serial.print("Encoder X ROTARY_ID1: "); Serial.println(encoderX.GetRotaryValue(ROTARY_X_ID1));
//      Serial.print("Encoder Y ROTARY_ID1: "); Serial.println(encoderY.GetRotaryValue(ROTARY_Y_ID1));
//      Serial.println("---------------------------------------");
//
//      updateSketchPaintBuffer(encoderX.GetRotaryValue(ROTARY_X_ID1), encoderY.GetRotaryValue(ROTARY_Y_ID1));
//    }
//    threads.yield();
//  }
//}
//
//void resetPosition() {
//  //POSITION_X = INITIAL_X;
//  //POSITION_Y = INITIAL_Y;
//  encoderX.SetRotary(ROTARY_X_ID1);
//  encoderY.SetRotary(ROTARY_Y_ID1);
//  Serial.println(F("|     - Position reset -        |"));
//}
//
//void printPosition() {
//  Serial.print(F("        X: "));
//  Serial.print(encoderX.GetRotaryValue(ROTARY_X_ID1));
//  Serial.print(F(" | Y: "));
//  Serial.println(encoderY.GetRotaryValue(ROTARY_Y_ID1));
//  Serial.println(F("|-------------------------------|"));
//}
