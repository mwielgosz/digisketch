/**
    @filename   :   digisketch.ino
    @brief      :   DigiSketch Arduino sketch
    @author     :   Mike Wielgosz

    Copyright (C) Mike Wielgosz     2019-03-12

   Permission is hereby granted, free of charge, to any person obtaining a copy
   of this software and associated documnetation files (the "Software"), to deal
   in the Software without restriction, including without limitation the rights
   to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
   copies of the Software, and to permit persons to  whom the Software is
   furished to do so, subject to the following conditions:

   The above copyright notice and this permission notice shall be included in
   all copies or substantial portions of the Software.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   FITNESS OR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
   LIABILITY WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
   THE SOFTWARE.
*/

#include <Arduino.h>
#include <Bounce2.h>
#define ENCODER_USE_INTERRUPTS
#define ENCODER_OPTIMIZE_INTERRUPTS
#include <Encoder.h>
#include <SD.h>
#include <SPI.h>
#include "TeensyThreads.h"
#include "epd4in2-digisketch.h"
#include "imagedata-digisketch.h"
#include "epdpaint-digisketch.h"

// Main
#define DEBUG       true
// Colors
#define COLORED     0
#define UNCOLORED   1
#define PIN_CL_X    14
#define PIN_DT_X    16
#define PIN_SW_X    17
#define PIN_CL_Y    20
#define PIN_DT_Y    22
#define PIN_SW_Y    23

Encoder knobX(PIN_CL_X, PIN_DT_X);
Encoder knobY(PIN_CL_Y, PIN_DT_Y);
Bounce switchX = Bounce();
Bounce switchY = Bounce();

Epd epd;

const char* sdPath = "dsketch"; // SDcard directory

byte dtOffset = 4; // Offset for rotary encoder detents (how many movements per detent)
short positionX = (EPD_WIDTH / 2) * dtOffset, positionY = (EPD_HEIGHT / 2) * dtOffset;

boolean IS_EPD_SLEEP = true;
boolean UI_UPDATE_LOCK = true;
boolean MENU_OPEN = false;
byte OPT_SELECTED = 0;

unsigned char IMAGE_BUF[16000];
Paint PAINT_SKETCH(IMAGE_BUF, EPD_WIDTH, EPD_HEIGHT);    //width should be the multiple of 8
unsigned char MENU_BUF[8000];
Paint PAINT_MENU(MENU_BUF, EPD_WIDTH / 2, EPD_HEIGHT / 2);

void setup() {
  Serial.begin(115200);
  while (!Serial) ;
  Serial.println(F("---------------------------------"));
  Serial.println(F("|     Welcome to DigiSketch     |"));
  Serial.println(F("---------------------------------"));
  Serial.println(F("|         ...LOADING...         |"));
  Serial.println(F("|-------------------------------|"));

  // Setup the X button with an internal pull-up :
  pinMode(PIN_SW_X, INPUT_PULLUP);
  // Setup the X encoder Bounce instance:
  switchX.attach(PIN_SW_X);
  switchX.interval(150); // interval in ms

  // Setup the Y button with an internal pull-up :
  pinMode(PIN_SW_Y, INPUT_PULLUP);
  // Setup the Y encoder Bounce instance:
  switchY.attach(PIN_SW_Y);
  switchY.interval(150); // interval in ms

  // Write initial positions to encoders (middle of screen)
  knobX.write(positionX);
  knobY.write(positionY);

  Serial.println(F("| Rotary Encoders init complete |"));
  Serial.println(F("|-------------------------------|"));

  // Initialize SDCARD
  initSdCard();
  if (DEBUG) {
    listSketchFiles();
  }

  // Initiaize e-paper
  initEpd(true);

  // Show boot logo
  epd.DisplayFrame(IMAGE_DIGISKETCH_LOGO);
  delay(5000);

  // Clear screen from SRAM
  epd.ClearFrame();

  // Setup Paint object buffer & Draw initial pixel
  PAINT_SKETCH.SetWidth(EPD_WIDTH);
  PAINT_SKETCH.SetHeight(EPD_HEIGHT);
  PAINT_SKETCH.Clear(UNCOLORED);
  PAINT_SKETCH.DrawPixel(positionX, positionY, COLORED);
  updateSketchOnEpd(false);

  Serial.println(F("|     Ready to DigiSketch!      |"));

  // Start UI Update (sketch) thread
  threads.addThread(updateSketchThread);

  Serial.println(F("+-------------------------------+"));
  Serial.println(F("|   Starting sketch draw loop   |"));
  Serial.println(F("|              ---              |"));
  Serial.println(F("| Push left knob to reset X & Y |"));
  Serial.println(F("| Push right knob to open menu  |"));
  Serial.println(F("|-------------------------------|"));
}

void loop() {
  short readX, readY;
  readX = knobX.read() / dtOffset;
  readY = knobY.read() / dtOffset;
  switchX.update();
  switchY.update();

  // Everything is in bounds & new position - DRAW
  if (readX != positionX || readY != positionY) {
    if (!MENU_OPEN) {
      if ((readX >= 0 && readX < EPD_WIDTH)
          && (readY >= 0 && readY < EPD_HEIGHT)) {
        positionX = readX;
        positionY = readY;
        if (DEBUG) {
          printPosition(positionX, positionY);
        }
        updateSketchPaintBuffer(positionX, positionY);
      } else {
        // X out of bounds
        if (readX < 0) {
          knobX.write(0);
          positionX = 0;
          if (DEBUG) {
            Serial.println("X under min");
            printPosition(positionX, positionY);
          }
        } else if (readX > EPD_WIDTH) {
          knobX.write((EPD_WIDTH * dtOffset) - 1);
          positionX = EPD_WIDTH - 1;
          if (DEBUG) {
            Serial.println("X over max");
            printPosition(positionX, positionY);
          }
        }
        // Y out of bounds
        if (readY < 0) {
          knobY.write(0);
          positionY = 0;
          if (DEBUG) {
            Serial.println("Y under min");
            printPosition(positionX, positionY);
          }
        } else if (readY > EPD_HEIGHT) {
          knobY.write((EPD_HEIGHT * dtOffset) - 1);
          positionY = EPD_HEIGHT - 1;
          if (DEBUG) {
            Serial.println("Y over max");
            printPosition(positionX, positionY);
          }
        }
      }
    }
  }
  // Left button press
  if (switchX.fell()) {
    if (DEBUG) {
      Serial.println(F("X button pressed"));
    }
    if (MENU_OPEN) {
      MENU_OPEN = false;
      knobY.write(positionY);
      epd.ClearFrame();
      updateSketchOnEpd(false);
    } else {
      /*
           TODO: Remove EPD SLEEP function & replace with resetPosition()
                 For DEBUGGING only
      */
      //resetPosition();

      if (IS_EPD_SLEEP) {
        initEpd(false);
      } else {
        /* Deep sleep */
        Serial.println(F("|         Sleeping EPD          |"));
        updateSketchOnEpd(false);
        epd.Sleep();
        IS_EPD_SLEEP = true;
        Serial.println(F("|         - EPD Sleep -         |"));
        Serial.println(F("+-------------------------------+"));
      }
    }
  }
  // Right button press
  if (switchY.fell()) {
    if (DEBUG) {
      Serial.println(F("Y button pressed - MENU OPEN"));
    }
    // MENU OPEN - from sketch
    MENU_OPEN = true;
    UI_UPDATE_LOCK = true; // Ensure UI thread is locked
    menuSelectReset(false);
    knobY.write(0);
    short int menuRead = 0;
    byte menuPosition = 0;
    do {
      menuRead = -(knobY.read() / dtOffset);
      switchX.update();
      switchY.update();

      if (menuRead != menuPosition) {
        menuPosition = menuRead;

//        if (DEBUG) {
//          Serial.print(F("knobY rd A:     ")); Serial.println(-knobY.read());
//          Serial.print(F("menuRead A:     ")); Serial.println(menuRead);
//          Serial.print(F("menuPosition A: ")); Serial.println(menuPosition);
//        }

        // Wrap menu selection
        if (menuRead > 2) {
          knobY.write(0);
          menuRead = 0;
          menuPosition = 0;
        } else if (menuRead < 0) {
          knobY.write(2 * dtOffset);
          menuRead = 2;
          menuPosition = 2;
        }
//        if (DEBUG) {
//          Serial.print(F("knobY rd A:     ")); Serial.println(-knobY.read());
//          Serial.print(F("menuRead B:     ")); Serial.println(menuRead);
//          Serial.print(F("menuPosition B: ")); Serial.println(menuPosition);
//        }
        selectMenu(menuPosition);
      }
      // Close menu
      if (switchX.fell()) {
        if (DEBUG) {
          Serial.println(F("X button pressed - CLOSE MENU"));
        }
        closeMenu();
      }
      // Perform function for selected menu item
      if (switchY.fell()) {
        // Select menu option
        switch (menuPosition) {
          case 0: // RESET
            resetSketch();
            break;
          case 1: // SAVE
            saveSketchFile();
            break;
          case 2: // LOAD
            loadSketchFile();
            break;
        }
      }
    } while (MENU_OPEN);
  }
}

void resetPosition() {
  knobX.write((EPD_WIDTH / 2) * dtOffset);
  knobY.write((EPD_HEIGHT / 2) * dtOffset);
  positionX = (EPD_WIDTH / 2) * dtOffset;
  positionY = (EPD_HEIGHT / 2) * dtOffset;
  Serial.println(F("|     - Position reset -        |"));
}

void printPosition(short x, short y) {
  Serial.print(F("        X: ")); Serial.print(x);
  Serial.print(F(" | Y: ")); Serial.println(y);
  Serial.println(F("|-------------------------------|"));
}
