/**
    @filename   :   epd_functions.ino
    @brief      :   DigiSketch Waveshare EPD function sketch
    @author     :   Mike Wielgosz

    Copyright (C) Mike Wielgosz     2019-03-12

   Permission is hereby granted, free of charge, to any person obtaining a copy
   of this software and associated documnetation files (the "Software"), to deal
   in the Software without restriction, including without limitation the rights
   to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
   copies of the Software, and to permit persons to  whom the Software is
   furished to do so, subject to the following conditions:

   The above copyright notice and this permission notice shall be included in
   all copies or substantial portions of the Software.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   FITNESS OR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
   LIABILITY WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
   THE SOFTWARE.
*/

void initEpd(boolean clearSRAM) {
  Serial.println(F("|     - Waking up e-paper -     |"));
  if (epd.Init() != 0) {
    Serial.println(F("|      E-Paper init failed      |"));
    IS_EPD_SLEEP = true;
    return;
  } else {
    //delay(1000);
    if (clearSRAM) {
      epd.ClearFrame(); /* This clears the SRAM of the e-paper display */
      //delay(500);
    } else {
      updateSketchOnEpd(false);
    }
    //delay(1000);
    //epd.DisplayFrame(); /* This displays the data from the SRAM in e-Paper module */
    //delay(2000);
    IS_EPD_SLEEP = false;
    //IDLE_MSEC = 0;
    Serial.println(F("|     E-Paper init complete     |"));
    Serial.println(F("+-------------------------------+"));
  }
}

void sleepEpd() {
  Serial.println(F("|     - Sleeping e-paper -      |"));
  IS_EPD_SLEEP = true;
  //delay(500);
  epd.DisplayFrame();
  //delay(2000);
  epd.Sleep();
  delay(1000);
  Serial.println(F("|        - EPD Sleeing -        |"));
  Serial.println(F("+-------------------------------+"));
}

void updateSketchPaintBuffer(int x, int y) {
  // Update Sketch Paint buffer
  PAINT_SKETCH.DrawPixel(x, y, COLORED);
  UI_UPDATE_LOCK = false;
}

void updateSketchOnEpd(boolean quick) {
  //Serial.println(F("Updating Sketch"));
  epd.SetPartialWindow(PAINT_SKETCH.GetImage(), 0, 0, PAINT_SKETCH.GetWidth(), PAINT_SKETCH.GetHeight(), 2);
  if (quick) {
    epd.DisplayFrameQuick();
  } else {
    epd.DisplayFrame();
  }
}

void updateMenuOnEpd(boolean quick) {
  epd.SetPartialWindow(PAINT_SKETCH.GetImage(), 0, 0, PAINT_SKETCH.GetWidth(), PAINT_SKETCH.GetHeight(), 2);
  epd.SetPartialWindow(PAINT_MENU.GetImage(), (EPD_WIDTH / 2) - (PAINT_MENU.GetWidth() / 2), (EPD_HEIGHT / 2) - (PAINT_MENU.GetHeight() / 2), PAINT_MENU.GetWidth(), PAINT_MENU.GetHeight(), 2);
  if (quick) {
    epd.DisplayFrameQuick();
  } else {
    epd.DisplayFrame();
  }
}

void updateSketchThread() {
  while (1) {
    if (!UI_UPDATE_LOCK && !IS_EPD_SLEEP) {
      updateSketchOnEpd(true);
      UI_UPDATE_LOCK = true;
    }
    threads.yield();
  }
}

void resetSketch() {
  resetPosition();
  OPT_SELECTED = 0;
  MENU_OPEN = false;
  IS_EPD_SLEEP = false;
  UI_UPDATE_LOCK = true;
  epd.ClearFrame();
  epd.DisplayFrame();
  //delay(2000);
  PAINT_SKETCH.Clear(UNCOLORED);
  PAINT_SKETCH.DrawPixel(positionX, positionY, COLORED);
  updateSketchOnEpd(false);
}
