/**
    @filename   :   menu.ino
    @brief      :   DigiSketch Menu setup sketch
    @author     :   Mike Wielgosz

    Copyright (C) Mike Wielgosz     2019-03-12

   Permission is hereby granted, free of charge, to any person obtaining a copy
   of this software and associated documnetation files (the "Software"), to deal
   in the Software without restriction, including without limitation the rights
   to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
   copies of the Software, and to permit persons to  whom the Software is
   furished to do so, subject to the following conditions:

   The above copyright notice and this permission notice shall be included in
   all copies or substantial portions of the Software.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   FITNESS OR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
   LIABILITY WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
   THE SOFTWARE.
*/


void menuSelectReset(boolean quick) {
  PAINT_MENU.Clear(UNCOLORED);
  PAINT_MENU.DrawRectangle(0, 0, (EPD_WIDTH / 2) - 1, (EPD_HEIGHT / 2) - 1, COLORED);
  PAINT_MENU.DrawRectangle(1, 1, (EPD_WIDTH / 2) - 2, (EPD_HEIGHT / 2) - 2, COLORED);

  PAINT_MENU.DrawFilledRectangle(8, 8, (EPD_WIDTH / 2) - 9, ((EPD_HEIGHT / 2) - 9) / 3, COLORED); // SELECTED
  PAINT_MENU.DrawStringAt(60, 18, "RESET", &Font24, UNCOLORED); // SELECTED

  PAINT_MENU.DrawRectangle(8, (((EPD_HEIGHT / 2) - 9) / 3) + 6, (EPD_WIDTH / 2) - 9, (((EPD_HEIGHT / 2) - 9) / 3) * 2, COLORED);
  PAINT_MENU.DrawStringAt(70, (((EPD_HEIGHT / 2) - 9) / 3) + 18, "SAVE", &Font24, COLORED); // UNSELECTED

  PAINT_MENU.DrawRectangle(8, ((((EPD_HEIGHT / 2) - 16) / 3) * 2) + 12, (EPD_WIDTH / 2) - 9, (EPD_HEIGHT / 2) - 9, COLORED);
  PAINT_MENU.DrawStringAt(70, ((((EPD_HEIGHT / 2) - 16) / 3) * 2) + 24, "LOAD", &Font24, COLORED); // UNSELECTED
  updateMenuOnEpd(quick);
}

void menuSelectSave(boolean quick) {
  PAINT_MENU.Clear(UNCOLORED);
  PAINT_MENU.DrawRectangle(0, 0, (EPD_WIDTH / 2) - 1, (EPD_HEIGHT / 2) - 1, COLORED);
  PAINT_MENU.DrawRectangle(1, 1, (EPD_WIDTH / 2) - 2, (EPD_HEIGHT / 2) - 2, COLORED);

  PAINT_MENU.DrawRectangle(8, 8, (EPD_WIDTH / 2) - 9, ((EPD_HEIGHT / 2) - 9) / 3, COLORED); // UNSELECTED
  PAINT_MENU.DrawStringAt(60, 18, "RESET", &Font24, COLORED); // UNSELECTED

  PAINT_MENU.DrawFilledRectangle(8, (((EPD_HEIGHT / 2) - 9) / 3) + 6, (EPD_WIDTH / 2) - 9, (((EPD_HEIGHT / 2) - 9) / 3) * 2, COLORED);
  PAINT_MENU.DrawStringAt(70, (((EPD_HEIGHT / 2) - 9) / 3) + 18, "SAVE", &Font24, UNCOLORED); // SELECTED

  PAINT_MENU.DrawRectangle(8, ((((EPD_HEIGHT / 2) - 16) / 3) * 2) + 12, (EPD_WIDTH / 2) - 9, (EPD_HEIGHT / 2) - 9, COLORED);
  PAINT_MENU.DrawStringAt(70, ((((EPD_HEIGHT / 2) - 16) / 3) * 2) + 24, "LOAD", &Font24, COLORED); // UNSELECTED
  updateMenuOnEpd(quick);
}

void menuSelectLoad(boolean quick) {
  PAINT_MENU.Clear(UNCOLORED);
  PAINT_MENU.DrawRectangle(0, 0, (EPD_WIDTH / 2) - 1, (EPD_HEIGHT / 2) - 1, COLORED);
  PAINT_MENU.DrawRectangle(1, 1, (EPD_WIDTH / 2) - 2, (EPD_HEIGHT / 2) - 2, COLORED);

  PAINT_MENU.DrawRectangle(8, 8, (EPD_WIDTH / 2) - 9, ((EPD_HEIGHT / 2) - 9) / 3, COLORED); // UNSELECTED
  PAINT_MENU.DrawStringAt(60, 18, "RESET", &Font24, COLORED); // UNSELECTED

  PAINT_MENU.DrawRectangle(8, (((EPD_HEIGHT / 2) - 9) / 3) + 6, (EPD_WIDTH / 2) - 9, (((EPD_HEIGHT / 2) - 9) / 3) * 2, COLORED);
  PAINT_MENU.DrawStringAt(70, (((EPD_HEIGHT / 2) - 9) / 3) + 18, "SAVE", &Font24, COLORED); // UNSELECTED

  PAINT_MENU.DrawFilledRectangle(8, ((((EPD_HEIGHT / 2) - 16) / 3) * 2) + 12, (EPD_WIDTH / 2) - 9, (EPD_HEIGHT / 2) - 9, COLORED);
  PAINT_MENU.DrawStringAt(70, ((((EPD_HEIGHT / 2) - 16) / 3) * 2) + 24, "LOAD", &Font24, UNCOLORED); // SELECTED
  updateMenuOnEpd(quick);
}

void selectMenu(byte menuOption) {
  switch (menuOption) {
    case 0:
      menuSelectReset(true);
      break;
    case 1:
      menuSelectSave(true);
      break;
    case 2:
      menuSelectLoad(true);
      break;
  }
}

void closeMenu() {
  MENU_OPEN = false;
  knobY.write(positionY * dtOffset);
  epd.ClearFrame();
  updateSketchOnEpd(false);
}
